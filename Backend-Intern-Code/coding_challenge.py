from flask import Flask, jsonify, request
import json 
import math

#dictionary of global battle room
b_room={}

class Player:
	def __init__(self, playerid, score):
		self.playerid_=playerid
		self.score_=score

class Battleroom:
	def __init__(self, battleroomid):
		self.battleroomid_=battleroomid
		self.players=[]
	def add_player(self,player):
		self.players.append(player)
	def get_rank(self,playerid):
		#sorts the list
		self.order_list()

		#find index of given player
		for index, player in enumerate(self.players):
			if(player.playerid_== playerid):
				break
		'''
		once the player is found the index is the position on the list, and because the list 
		is already sorted in descending order, the index+1 of the list will be its rank because
		list begins at 0 index
		''' 
		r=index+1

		#returns rank and total number of players
		return r

	def get_median(self):
		#sorts the list 
		self.order_list()

		size=self.get_size()

		#condition for if size of the list is an even number
		if(size==1):
			median_score=int(self.players[0].score_)
			return median_score
		else:
			if(size%2==0):
				median_rightind=int(size/2)
				median_leftind=median_rightind-1

				#adds the score of left and right and divide by 2
				medianright_score=int(self.players[median_rightind].score_)
				medianleft_score=int(self.players[median_leftind].score_)
				median_score=(medianright_score+medianleft_score)/2
				return median_score
			#condition if size of list is an odd number
			else:
				median_index=math.floor(size/2)
				median=self.players[median_index]
				return median.score_


	def get_size(self):
		return len(self.players)

	#sorts the list of players in descending order
	def order_list(self):
		return self.players.sort(key=lambda x:x.score_, reverse=True)

app = Flask(__name__)

@app.route('/rank/', methods=['GET', 'POST'])
def ranking():
	if(request.method =='POST'):
		#retrieves POST msg
		json_msg=request.get_json()

		#grabs value of json message
		b_id=json_msg['battleroom_id']
		p_id=json_msg['player_id']
		score_=json_msg['score']

		#checks to see if a battleroomid exists in dictionary
		if b_id in b_room.keys():
			#append player to list in battleroom obj
			p=Player(p_id,score_)
			#access obj in dictionary and add player to list using method
			b_room[b_id].add_player(p)

			#now retrieve their ranking 
			rank=b_room[b_id].get_rank(p.playerid_)
			totalsize=b_room[b_id].get_size()
			rank_string=str(rank) + "/" + str(totalsize)
			return jsonify({'Rank': rank_string})

		else:
			#create new battleroom obj and add player
			b_room[b_id]=Battleroom(b_id)
			p=Player(p_id,score_)
			b_room[b_id].add_player(p)

			return jsonify({'Rank:': '1/1'})
			



	else:
		return jsonify({"About": "Hello World!"})

@app.route('/median/', methods=['GET', 'POST'])
def median():
	if(request.method =='POST'):
		#retrieves POST msg
		json_msg=request.get_json()

		#grabs value of json message
		b_id=json_msg['battleroom_id']
		p_id=json_msg['player_id']
		score_=json_msg['score']

		#checks to see if a battleroomid exists in dictionary
		if b_id in b_room.keys():
			#append player to list in battleroom obj
			p=Player(p_id,score_)
			#access obj in dictionary and add player to list using method
			b_room[b_id].add_player(p)
			median_score=b_room[b_id].get_median()
			
			#returns player id, score of given json obj and median score of battleroom
			return jsonify({"player_id": p.playerid_, "score": p.score_, "median_score": str(median_score)})

		else:
			#create new battleroom obj and add player
			b_room[b_id]=Battleroom(b_id)
			p=Player(p_id,score_)
			b_room[b_id].add_player(p)

			median_score=b_room[b_id].get_median()
			
			#returns player id, score of given json obj and median score of battleroom
			return jsonify({"player_id": p.playerid_, "score": p.score_, "median_score": str(median_score)})


if __name__ == '__main__':
	app.run(debug=True)





