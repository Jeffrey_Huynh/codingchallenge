# Engineer.Backend.Intern 
## How to Use
1.run the code python coding_challenge.py

2. when testing for rank:
    curl -H "Content-Type: application/json" -X POST -d '{"player_id" : "1", "battleroom_id": "1", "score": "100"}' http://127.0.0.1:5000/rank/
    or whatever localhost it gives you upon running the python file
3. when testing for median:
    curl -H "Content-Type: application/json" -X POST -d '{"player_id" : "1", "battleroom_id": "1", "score": "100"}' http://127.0.0.1:5000/median/
or whatever localhost it gives you upon running the python file
    
## Questions
1. I spent one day on the coding challenge, completed User Story 1 and part of User 2(median retrieval)
2. If I had more time on the coding challenge, I would have implemented the displayal of the full leaderboard.I would also add conditions for edge cases. For instance, what would the program do there was insertion of data with the same playerid or multiple insertions of the same exact information. I wouldn't want to add this copy in my list because it has already been added before. I would look for cases like this and add the neccessary steps to deal with these conditions
3. The primary technology that I used was Flask. When using Flask I found that it was very easy to use and it was flexible. However some downsides to it is that it is limited in features and support documentation. In terms of the design of my program, the way that I organized my ranking look up was by creating a dictionary and creating key, value pairs for the battleroom_id and a battleroom object. Then in order to calculate the rank of a specific player I stored the players in a list, sorted the list in descending order, and looked for the index of the player. I used a dictionary because it made for fast lookups for given battleroom_ids. Although this was good for lookup speed, I realized that if there were a large number battlerooms it could potentially require a significant amount of memory space. So, implementing a database may be optimal. Also, when actually calculating the rankings of the given players, I would append the player to the end of my list of players, sort the list, and then loop through the list again to find the corresponding index of the given player id. With this, I am optimizing the time it takes to insert a player. However, sorting the list at best would have a speed of O(n*log(n)), and then find the index of the player would potentially take O(n) time. This might not be optimal when dealing with a large number of players in a given battleroom. One thing I could potentially do is to insert the players in order of their ranking, and inside the insertion function, keep track of its index and return its index. This way I would reduce the time complexity from O(n * log(n)) to just O(n) because I wouldn't have sort through the list and loop through it again. 
4. As mentioned previously I would test for edge cases such as what would happend if there were multiple inputs of the same data and inputs of pre-existing values of a given playerid. I would also utilize unit testing on the my defined functions to see if they outputted the correct data.
5. I would create another field in the player class for username, then I would initialize my player objects with the previous information and the new username field. After that I would create a method to find the playerid given a players username(assuming that the previous information is still given i.e player_id, battleroom_id, score) and then use my pre-existing methods the calculate the rank and median score
## Design Documentation
Class Player
fields: playerid_, score_

Class Battleroom
fields: battleroomid_ , players [ ]

methods: 

    constructor (battleroomid) 
        intializes battleroomid_ to parameter
        intializes empty list of Player objs
        
    add_player(player)
        adds player to list
        
    get_rank(playerid)
        given player id, sorts the list, finds according playerid and returns the rank, rank should be index of player 
        in the list plus one because the list starts at 0 index
        
    get_median()
        returns the median score of the list of players
        
    get_size()
        returns size of the list of players
        
    order_list()
        orders the list in descending order

b_room
    dictionary of Battleroom objects, where key is the battleroom id(str)
    allows for keeping track of multiple battlerooms


